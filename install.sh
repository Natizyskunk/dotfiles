#!/bin/bash

cd ~

# Install neofetch
apt install -y neofetch

# Download all .dotfiles
dotconfigs=".bash_aliases .bash_logout .bash_profile .bashrc .editorconfig .exports .extra .functions .fzf.bash .fzf.zsh .gitconfig .gitignore .gvimrc .hushlogin .inputrc .lesshst .nanorc .npmrc .p10k.zsh .profile .screenrc .shell.pre-oh-my-zsh .sudo_as_admin_successful .viminfo .vimrc .wgetrc .wslconfig"
for dotconfig in $dotconfigs; do
  wget "https://gitlab.com/Natizyskunk/dotfiles/raw/master/${dotconfig}"
done

mkdir .cargo
mkdir .cargo/bin
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/cargo-clippy.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/cargo-fmt.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/cargo-miri.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/cargo.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/clippy-driver.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/rls.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/rust-gdb.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/rust-gdbgui.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/rust-lldb.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/rustc.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/rustdoc.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/rustfmt.exe"
wget -P .cargo/bin/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.cargo/bin/rustup.exe"

mkdir .config
mkdir .config/bottom
mkdir .config/bpytop
mkdir .config/composer
mkdir .config/template
mkdir .config/htop
mkdir .config/neofetch
mkdir .config/nvim
mkdir .config/nvim/after
mkdir .config/nvim/after/plugin
mkdir .config/nvim/colors
wget -P .config/bottom "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/bottom/bottom.toml"
wget -P .config/bpytop "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/bpytop/bpytop.conf"
wget -P .config/composer "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/composer/.htaccess"
wget -P .config/git/template "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/git/template/HEAD"
wget -P .config/htop "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/htop/htoprc"
wget -P .config/neofetch "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/neofetch/config.conf"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/autopairs.rc.lua"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/completion.rc.vim"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/defx.rc.vim"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/fugitive.rc.vim"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/lsp-colors.rc.vim"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/lspconfig.rc.vim"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/lspsaga.rc.vim"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/lualine.rc.lua"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/tabline.rc.vim"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/telescope.rc.vim"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/treesitter.rc.vim"
wget -P .config/nvim/after/plugin "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/after/plugin/web-devicons.rc.vim"
wget -P .config/nvim/colors "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/colors/NeoSolarized"
wget -P .config/nvim "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/init.vim"
wget -P .config/nvim "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/maps.vim"
wget -P .config/nvim "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.config/nvim/plug.vim"

mkdir .ssh
wget -P .ssh/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.ssh/config"
wget -P .ssh/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.ssh/known_hosts"

mkdir .vim
mkdir .vim/autoload
mkdir .vim/colors
mkdir .vim/config
mkdir .vim/syntax
wget -P .vim/autoload/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.vim/autoload/pathogen.vim"
colors="NeoSolarized.vim solarized.vim tomorrow-noght.vim tomorrow.vim zenburn.vim"
for color in $colors; do
  wget -P .vim/colors/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.vim/colors/${color}"
done
wget -P .vim/config/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.vim/autoload/settings.vim"
wget -P .vim/syntax/ "https://gitlab.com/Natizyskunk/dotfiles/raw/master/.vim/syntax/json.vim"

# Downlaod and install pihole-gravity-wildcards & pihole-gravity-optimise scripts
# wget -qO /usr/local/bin/generateGravityWildcards.sh https://raw.githubusercontent.com/mmotti/pihole-gravity-wildcards/master/generateGravityWildcards.sh
# wget -qO /usr/local/bin/gravityOptimise.sh https://raw.githubusercontent.com/mmotti/pihole-gravity-optimise/master/gravityOptimise.sh
# chmod +x /usr/local/bin/generateGravityWildcards.sh /usr/local/bin/gravityOptimise.sh

# Create cron files for pihole-gravity-wildcards & pihole-gravity-optimise scripts
# sudo touch /etc/cron.d/generateGravityWildcards
# sudo touch /etc/cron.d/gravityOptimise
# echo '30 3 * * * root PATH="$PATH:/usr/local/bin/"' generateGravityWildcards.sh >> /etc/cron.d/generateGravityWildcards
# echo '45 3 * * * root PATH="$PATH:/usr/local/bin/"' gravityOptimise.sh >> /etc/cron.d/gravityOptimise

# source .bashrc
source .zshrc

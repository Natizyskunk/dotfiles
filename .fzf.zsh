# Setup fzf
# ---------
if [[ ! "$PATH" == */home/natizyskunk/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/natizyskunk/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/natizyskunk/.fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/home/natizyskunk/.fzf/shell/key-bindings.zsh"

## Dotfiles  

### Installation

**a)** auto installation.
```bash
curl -sSL https://gitlab.com/Natizyskunk/dotfiles/raw/master/install.sh | bash
```

**b)** manual installation.
```bash
cd ~
wget https://gitlab.com/Natizyskunk/dotfiles/raw/master/install.sh
chmod +x install.sh
./install.sh
```

# ~/.profile: executed by Bourne-compatible login shells.

# if running bash
# if [ "$BASH" ]; then
#   if [ -f "$HOME/.bashrc" ]; then
# 	  . "$HOME/.bashrc"
#   fi
# fi

# if running zshrc
if [ -f "$HOME/.zshrc" ]; then
. "$HOME/.zshrc"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi
. "$HOME/.cargo/env"

mesg n || true

echo -------------------------
echo

neofetch

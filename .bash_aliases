### ALIASES ###

# Laravel Sail 
alias sail="[ -f sail ] && bash sail || bash vendor/bin/sail"

# Basic stuffs
export LS_OPTIONS='--show-control-chars -F'
alias l="lsd $LS_OPTIONS -C $*"
alias ls="lsd $LS_OPTIONS $*"
alias la="lsd $LS_OPTIONS -lA $*"
alias ll="lsd $LS_OPTIONS -l -gohlat $*"
	
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	# You may uncomment the following lines if you want `ls' to be colorized:
	
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias l="lsd --color=auto $LS_OPTIONS -C $*"
    alias ls="lsd --color=auto $LS_OPTIONS $*"
    alias la="lsd --color=auto $LS_OPTIONS -lA $*"
    alias ll="lsd --color=auto $LS_OPTIONS -l -gohlat $*"
	
    alias dir="dir --color=auto"
    alias vdir="vdir --color=auto"
    
    alias grep="grep --color=auto"
    alias fgrep="fgrep --color=auto"
    alias egrep="egrep --color=auto"
fi

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias cls='clear'
alias www='cd /var/www'
alias ping='ping -c 4'

# GIT commands
alias gs='git status'
alias gb='git branch'
alias gr='git remote -v'
alias gp='git pull origin $1'
alias gl='git log --oneline --all --graph --decorate $*'
alias grh='git reset --HARD'

# Maintenance commands
alias update='apt update -y'
alias upgrade='apt upgrade -y'
alias dist-upgrade='apt dist-upgrade -y'
alias autoremove='apt autoremove --purge'
alias autoclean='apt autoclean'
alias sysup='update && upgrade && dist-upgrade && autoremove && autoclean'
alias install='apt install -y'
